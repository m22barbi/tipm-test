import * as fs from 'fs';
import * as path from 'path'

const data = fs.readFileSync(path.join(__dirname, '../rawData.csv'))
const rows = data.toString().split('\n')
const labels = rows.shift()?.split(',')

if(labels?.length !== 5) throw new Error('Invalid file format')

interface Data {
    step: number
    pc1: number
    pc2: number
    pc3: number
    label: -1
}

const parsedData : Data[] = []

for(const row of rows) {
    //on ignore les lignes vide
    if(!row.trim()) continue
    const rowValues = row.split(',')
    const parsedRow : Data = {
        step: parseInt(rowValues[0]),
        pc1: parseFloat(rowValues[1]),
        pc2: parseFloat(rowValues[2]),
        pc3: parseFloat(rowValues[3]),
        label: -1
    }
    parsedData.push(parsedRow)
}

// nombre de segment pour le découpage
const bins = 10

// méthode n° 1 par largeur
// il nous faut définir la valeur maximum et minimum
const pc1 = parsedData.map(e => e.pc1)

const max = pc1.reduce((prev, cur) => prev > cur ? prev : cur)
const min = pc1.reduce((prev, cur) => prev < cur ? prev : cur)

// une fois que c'est fait on peut calculer la largeur
// vu que l'on commence de 0 on retire 1
const width = (max - min) / ( bins)

// maintenant il y a 4 methodes
// - 1: par catégorisation (les valeur sont remplacé par le numero de l'intervale)
// - 2: par moyene ( les valeurs sont remplacé par la moyene de l'intervale )
// - 3: par mediane ( les valeurs sont remplcé par la médiane de l'intervale )
// - 4: par bordure ( les valeurs sont remplacé par le bord le plus proche de l'intervale)

// vu que dans le programme ils on utilisé la methode 1 c'est celle que je vais faire
//on calcul le numero du segment
    // on ajoute min affin d'être sur un intervale positif
const discreteData = pc1.map(value => Math.floor((value - min) / width))

console.log(JSON.stringify(discreteData))

const summary = {
    '0': discreteData.reduce((prev, next) => next == 0 ? prev + 1 : prev, 0),
    '1': discreteData.reduce((prev, next) => next == 1 ? prev + 1 : prev, 0),
    '2': discreteData.reduce((prev, next) => next == 2 ? prev + 1 : prev, 0),
    '3': discreteData.reduce((prev, next) => next == 3 ? prev + 1 : prev, 0),
    '4': discreteData.reduce((prev, next) => next == 4 ? prev + 1 : prev, 0),
    '5': discreteData.reduce((prev, next) => next == 5 ? prev + 1 : prev, 0),
    '6': discreteData.reduce((prev, next) => next == 6 ? prev + 1 : prev, 0),
    '7': discreteData.reduce((prev, next) => next == 7 ? prev + 1 : prev, 0),
    '8': discreteData.reduce((prev, next) => next == 8 ? prev + 1 : prev, 0),
    '9': discreteData.reduce((prev, next) => next == 9 ? prev + 1 : prev, 0),
    '10': discreteData.reduce((prev, next) => next == 10 ? prev + 1 : prev, 0),
    'mean': discreteData.reduce((prev, next) => prev + next) / discreteData.length,
    min,
    max,
    width
}

console.log("cas normale")
console.log(summary)

export { discreteData, summary }