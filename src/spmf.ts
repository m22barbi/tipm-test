// ce fichier a pour but de miner les pattern

import { exec } from 'child_process'
import { readFileSync, writeFileSync } from 'fs'
import path = require('path')
import { windows } from './fenêtrage'

const ramgigs = 4
const minsup = 40

function convertToPrefixSpan(windows: number[][]) {
    const datas = windows.map(e => e.map(v => v + 1).join(' -1 ') + ' -1 -2')
    return datas.join('\n')
}


const eclat = convertToPrefixSpan(windows)
console.log(eclat)
writeFileSync('/tmp/spmfinput.txt', eclat)

exec(`java -Xmx${ramgigs}G -jar ${path.join(__dirname, '../spmf.jar')} run PrefixSpan /tmp/spmfinput.txt /tmp/spmfoutput.txt ${minsup}%`, () => {
    const output = readFileSync('/tmp/spmfoutput.txt').toString().split('\n')

    output.pop()
    console.log(output.map(e => {
        const row = e.split(' -1')
        const sup = row.pop()
        return `{ seq: [${row.map(v => parseInt(v) - 1).join(', ')}], support: ${sup?.split('#SUP: ')[1]} }`
    }).join(',\n'))
})