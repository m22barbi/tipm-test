import { discreteData } from './discrétisation'

console.log("\n\ndémarage du fenêtrage")


const param = { width: 10, increment: 5, dataLength: discreteData.length }

console.log(param)

const test = ((discreteData.length + 1) / param.width)
if(test !== Math.floor(test)) {
    console.log('some aproximation will be made since the data is not divisible by the window length')
}

const windows: number[][] = []
let i = 0

while(true) {
    if(i + param.width/2 < discreteData.length) {
        windows.push(discreteData.slice(i, i + param.width))
        i += param.increment
    } else {
        windows.push(discreteData.slice(i))
        break
    }
}

//formatage de l'affichage il n'y a pas de traitement sur les données ici
console.log(JSON.stringify(windows, null, 0).replace(/\]\,\[/g, '],\n['))

export { windows }